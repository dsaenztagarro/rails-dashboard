class Comment < ActiveRecord::Base
	attr_accessible :body, :commentable_id, :commentable_type
	belongs_to :commentable, polymorphic: true
  
	validates_presence_of :body
	validates_length_of :body, :within => 10..2000  
end
