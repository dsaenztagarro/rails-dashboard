class Achievement < ActiveRecord::Base
  attr_accessible :title, :description, :id
  validates_presence_of :title, :description
  validates :title, uniqueness: true
  has_many :comments, :as => :commentable
end
