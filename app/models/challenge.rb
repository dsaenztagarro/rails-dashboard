class Challenge < ActiveRecord::Base
  attr_accessible :description, :image_url, :title
  has_many :comments, as: :commentable
end
