class Admin::TasksController < ApplicationController
  helper_method :sort_column, :sort_direction
  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 5, :page => params[:page])

    respond_to do |format|
      # Prevent layouts during AJAX requests!:  !request.xhr?
      format.html # index.html.erb
      format.json { render json: @tasks, :layout => !request.xhr? }
    end
  end

  # GET /tasks/list
  def list
    @tasks = Task.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 5, :page => params[:page])
    
    respond_to do |format|
      format.html { render :template => 'admin/tasks/_list', :layout => false }
    end
  end
  
  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task = Task.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(params[:task])

    respond_to do |format|
      if @task.save
        format.html { redirect_to [:admin,@task], notice: 'Task was successfully created.' }
        format.json { render json: @task, status: :created, location: @task }
      else
        format.html { render action: "new" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to [:admin,@task], notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to admin_tasks_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def sort_column
    Task.column_names.include?(params[:sort])? params[:sort] : "name"
  end
  
  def sort_direction
    #Avoid SQL injection
    %w[asc desc].include?(params[:direction])? params[:direction] : "asc"
    # If value is nil then assign "asc" value: params[:direction] ||= "asc"
  end
end
