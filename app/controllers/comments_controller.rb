class CommentsController < ApplicationController
  before_filter :find_commentable

  respond_to :json

  # GET /commentable/:id/comments.json
  def index
    respond_to do |format|
      format.html { render :layout => false } # index.html.haml
      format.json { render json: @commentable.comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # new_post_comment GET /posts/:post_id/comments/new
  # new_post_comment GET /posts/:post_id/comments/new.js
  def new
    @comment = Comment.new

    respond_to do |format|
      format.html { render :layout => false } # new.html.erb
      format.js #new.js.erb
    end
  end

  # GET /comments/1/edit
  def edit
    # do nothing
  end 
  
  # POST /commentable/:id/comments.json
  def create
    @comment = @commentable.comments.build(params[:comment])    
    if @comment.save
      respond_with @comment 
    else
      respond_with :nothing => true
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def find_commentable
    @commentable = nil
    params.each do |name,value|
      if name =~ /(.+)_id$/
        @commentable = $1.classify.constantize.find(value)
        return
      end
    end
  end

end
