class UsersController < ApplicationController
  
  # GET /users/new
  def new
    @user = User.new
  end
  
  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    if @user.save
      UserMailer.delay(:queue => 'notifications').register_confirmation(@user)
      flash[:notice] = "Congratulations, you are a new member. You will receive an email to activate your member login."
    else
      render :new
    end
  end
end
