class PublicController < ApplicationController
  # Render mobile or desktop depending on User-Agent for these actions.
  before_filter :check_for_mobile, :only => [:home]

  def home
#  - if user_signed_in?
#    = render :file => "/public/home"
  	@user = User.new
    respond_to do |format|
      format.html # home.html.erb
      format.js # home.js.erb
    end
  end

  def test
  end

  def help
  end

  def html_template
  	render :layout => false
  end
end
