app.factory 'AchievementsCommentsService', ($resource) ->
	$resource("/achievements/:achievement_id/comments/:comment_id", 
    {achievement_id: "@achievement_id", comment_id: "@comment_id"}) 
