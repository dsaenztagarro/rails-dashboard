app.factory 'EventBusService', ['$rootScope',($rootScope)->
  eventBusService = {}
  
  eventBusService.event= {}
  
  eventBusService.sendEvent = (code,data)->
    $rootScope.$broadcast code, data

  eventBusService
]
