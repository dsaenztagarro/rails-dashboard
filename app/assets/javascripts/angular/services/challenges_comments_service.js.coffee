app.factory 'ChallengesCommentsService', ($resource) ->
	$resource("/challenges/:challenge_id/comments/:comment_id", 
    {challenge_id: "@challenge_id", comment_id: "@comment_id"}) 
