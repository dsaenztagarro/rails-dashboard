app.factory 'UtilsService', ($resource) ->
  root = {}
  
  root.capitaliseFirstLetter = (string)->
    string.charAt(0).toUpperCase() + string.slice(1)
  
  root.pluralize = (string)->
    string + 's'

  root
