'use strict'

app.directive 'commentable', ($http,$compile,$parse)->
  {
    compile: (element, attrs) ->
      console.info 'directive:commentable:compileFn'
      html = "<a><i class='icon-comments'></i>Reply</a>"
      options = element.find('.actions')
      options.append(html)
      
      postLink = (scope, element, attrs) ->
        console.info 'directive:commentable:linkFn'
        scope.newComment = {}
        
        a = element.find('.icon-comments').parent()
        id = attrs.id
        
        a.bind 'click', ($event)->
          console.log 'directive:commentable:reply'
          $event.stopPropagation()
          scope.$apply -> scope.selected = true
          
          if not element.parent().find('#reply-'+attrs.id).length > 0
            $http.get('/comments/new').success (data,status,header,config)->
              console.debug 'directive:commentable:reply:id:', attrs.id
              li = $('<li id="reply-'+attrs.id+'"></li>').append($compile(data)(scope))
              li.hide()
              element.after(li)
              li.slideDown()
  }
