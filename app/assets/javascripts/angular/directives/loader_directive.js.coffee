'use strict'

app.directive 'loader', ['UtilsService', (UtilsService)->
  {
    priority: 100,
    compile: (element, attrs) ->

      postLink = (scope, element, attrs) ->
        expression = attrs.loader
        scope.loaderModel = scope.$eval(expression)
        scope.loaderModelType = UtilsService.capitaliseFirstLetter(expression)
        scope.loaderElement = element
  }
]
