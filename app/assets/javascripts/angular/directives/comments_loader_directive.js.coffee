'use strict'

app.directive 'commentsLoader', ['$http','$compile','UtilsService',
  ($http, $compile, UtilsService)->
    {
      priority: 50,
      link: (scope, element, attrs) ->

        scope.getServiceInstance= ->
          # Getting service name
          commentableType = UtilsService.pluralize(scope.loaderModelType)
          serviceName = commentableType+'CommentsService'

          # Injecting service
          injector = angular.injector(['lifeOnChartsApp','ng'])
          service = injector.get(serviceName)

        scope.getURLParams= ->
          commentableId = scope.loaderModelType.toLowerCase() + '_id'
          obj = {}
          obj[commentableId] = scope.loaderModel.id
          obj

        scope.getComments= ->
          service = scope.getServiceInstance()
          urlParams = scope.getURLParams()
          service.query(urlParams)

        scope.$on 'actionLoadComments', (event,data) ->
          if not event.defaultPrevented and
              scope.loaderModelType is data.actionModelType and
              scope.loaderModel.id is data.actionModel.id
            event.preventDefault()

            scope.comments = scope.getComments()

            config = {
              headers: { 'Accept': 'text/html', 'Content-Type': 'text/plain' }
            }
            $http.get('/comments',config).success (data,status,header,config)->
              html = angular.element(data)
              element = scope.loaderElement
              element.find('ul.comments').remove()
              element.append($compile(html)(scope))

        scope.saveComment= (achievementId,comment)->
          service = scope.getServiceInstance()
          urlParams = scope.getURLParams()
          service.save urlParams, comment, (newComment)->
            scope.comments.push(newComment)
    }
]
