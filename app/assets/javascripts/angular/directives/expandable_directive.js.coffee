'use strict'

app.directive 'expandable', ['EventBusService', (EventBusService)->
  {
    priority: 50,
    compile: (element, attrs) ->
      openBtn = angular.element "<a><i class='icon-chevron-down'></i>Open</a>"
      closeBtn =  angular.element "<a><i class='icon-chevron-up'></i>Close</a>"

      options = element.find('.actions')
      options.prepend openBtn
      options.prepend closeBtn

      postLink = (scope, element, attrs) ->
        scope.expanded = false
        scope.openBtn = element.find('.icon-chevron-down').parent()
        scope.closeBtn = element.find('.icon-chevron-up').parent()

        scope.$watch 'selected', (expandedAttr)->
          children = element.find '.children'

          if expandedAttr is true
            scope.openBtn.hide()
            scope.closeBtn.show()
            children.slideDown()
          else
            scope.openBtn.show()
            scope.closeBtn.hide()
            children.slideUp()

        scope.openBtn.bind 'click', ($event)->
          $event.stopPropagation()
          scope.$apply ->
            scope.selected = true
            data = {
              actionModel: scope.actionModel,
              actionModelType: scope.actionModelType,
            }
            EventBusService.sendEvent 'actionOpenDetails', data

        scope.closeBtn.bind 'click', ($event)->
          $event.stopPropagation()
          scope.$apply ->
            scope.selected = false
            EventBusService.sendEvent 'closeDetails'

  }
]
