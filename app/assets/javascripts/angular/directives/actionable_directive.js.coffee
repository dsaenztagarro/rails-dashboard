'use strict'

app.directive 'actionable', (UtilsService)->
  {
    priority: 100,
    compile: (element, attrs) ->

      postLink = (scope, element, attrs) ->
        scope.focus = false
        scope.selected = false
        expression = attrs.actionable
        scope.actionModel = scope.$eval(expression)
        scope.actionModelType = UtilsService.capitaliseFirstLetter(expression)

        element.bind 'mouseover', ($event)->
          $event.stopPropagation()
          scope.$apply -> scope.focus = true

        element.bind 'mouseout', ($event)->
          $event.stopPropagation()
          scope.$apply -> scope.focus = false

        scope.$watch 'focus || selected', ->
          show = scope.focus || scope.selected
          actions = element.find('.actions')
          if show
            actions.show()
          else
            actions.hide()
  }
