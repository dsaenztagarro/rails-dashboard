@app = angular.module("lifeOnChartsApp",['ngResource'])

app.config ['$httpProvider', ($httpProvider)->
  $httpProvider.defaults.headers.common['X-CSRF-Token'] =
    $('meta[name=csrf-token]').attr('content')
]

app.constant 'TabConstants', {
  LIST: "list",
  DETAILS: "details"
}
