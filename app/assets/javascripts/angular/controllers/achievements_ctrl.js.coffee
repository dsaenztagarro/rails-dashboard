app.controller 'AchievementsCtrl',
  ['$scope','$http','$compile',
  'AchievementsService','AchievementsCommentsService',
  ($scope, $http, $compile,
  AchievementsService, AchievementsCommentsService) ->
    $scope.achievements = AchievementsService.query()

    # To show list/details
    $scope.selection = "list"

    $scope.$on 'tabSelected', (event,data)->
      $scope.selection = data.selection

    $scope.$on 'actionOpenDetails', (event,data)->
      $scope.$broadcast 'test'
      if not event.defaultPrevented
        if data.actionModelType is 'Achievement'
          event.preventDefault()
          $scope.$broadcast 'actionLoadComments', data
  ]
