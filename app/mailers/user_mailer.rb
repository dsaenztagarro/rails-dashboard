class UserMailer < ActionMailer::Base
  default from: "dashboard@openshift.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.signup_confirmation.subject
  #
  def register_confirmation(user)
    @user = user
    mail to: user.email, subject: "Dashboard: Register confirmation"
  end

  def social_login(user)
    @user = user
    mail to: user.email, subject: "Dashboard: Login through social network"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Dashboard: Password reset"
  end

end