module StaticPagesHelper

	def icon_provider 
		case session[:provider] 
		when "twitter"
		  "icon-twitter"
		when "facebook"
		  "icon-facebook-sign"
		when "linkedin"
		  "icon-linkedin"
		when "github"
		  "icon-github"		  
		when "google_oauth2"
		  "icon-google-plus"
		else
		  "icon-user"
		end 
	end
end
