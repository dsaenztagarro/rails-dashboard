class RenameLoginToNickname < ActiveRecord::Migration
  def up
  	rename_column :users, :login, :nickname
  end

  def down
  	rename_column :users, :nickname, :login
  end
end
