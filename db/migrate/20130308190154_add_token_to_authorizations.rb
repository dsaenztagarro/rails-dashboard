class AddTokenToAuthorizations < ActiveRecord::Migration
  def change
  	add_column :authorizations, :token, :string
  	add_column :authorizations, :expires_at, :datetime
  end
end
