class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      t.text :body
      t.references :commentable, polymorphic: true
      t.timestamps
    end
  end
end
