class RenameAuthorizationsToAuthentications < ActiveRecord::Migration
  def up
  	rename_table :authorizations, :authentications
  end

  def down
  	rename_table :authentications, :authorizations
  end
end
