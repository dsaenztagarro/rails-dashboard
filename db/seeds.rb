require 'active_record/fixtures'

# Init data for production env

User.delete_all

ActiveRecord::Fixtures.create_fixtures("#{Rails.root}/spec/fixtures", "users")


Achievement.delete_all
Challenge.delete_all
Comment.delete_all

(1..10).each do |i|
  achievement = Achievement.create(
    :title       => "Achievement#{i}",
    :description => "Long description about achievement#{i}" )
  (1..5).each do |j|
    Comment.create(
      :body             => "Comment#{j} for achievement #{i}",
      :commentable_id   => achievement.id,
      :commentable_type => "Achievement" )
  end
end

(1..10).each do |i|
  challenge = Challenge.create(
    :title       => "Challenge#{i}",
    :description => "Long description about challenge#{i}" )
  (1..5).each do |j|
    Comment.create(
      :body             => "Comment#{j} for challenge#{i}",
      :commentable_id   => challenge.id,
      :commentable_type => "Challenge" )
  end
end
