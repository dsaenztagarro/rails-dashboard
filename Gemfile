source 'http://rubygems.org'

gem 'rails', '3.2.11'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'will_paginate' #Pain free coffeescript testing under Rails 3.1

gem 'haml'

gem 'mysql2'

gem 'debugger'

group :development do
  gem 'guard-rspec', '>=1.2.1'
  gem 'guard-spork', '>=1.2.0'
  gem 'haml-rails'
  gem 'rails_best_practices'
end

group :test do
  gem 'factory_girl_rails'
  gem 'cucumber-rails', '1.3.0', :require => false
  gem 'capybara', '>= 2.0.2'
  gem 'launchy', '>= 2.1.2'
  gem 'poltergeist'
  gem 'database_cleaner', '>= 0.9.1'
  gem 'spork', '0.9.2'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'cucumber', '1.2.5'
  gem 'jasmine-rails'
  gem 'simplecov'
  gem 'guard-cucumber'
  gem 'guard-jasmine'
  gem 'email_spec', '>= 1.4.0'
  # System dependent gems (Linux):
  gem 'rb-inotify', '0.8.8', :require => false
  gem 'rb-fsevent', :require => false
  gem 'rb-fchange', :require => false
  gem 'libnotify', '0.5.9'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  # Coffeescript preprocessor
  gem 'therubyracer'
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'yui-compressor'
  gem 'jquery-rails'
  gem 'jquery_mobile_rails'
  gem 'angular-rails'
end


# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

gem 'daemons'
gem 'delayed_job_active_record'

gem 'devise'
gem 'omniauth-facebook', '=1.4.0'
gem 'omniauth-google-oauth2'
gem 'omniauth-twitter'
gem 'omniauth-linkedin'
gem 'omniauth-github'

gem 'unicorn'
gem 'capistrano'

# The gem reads a config/application.yml file and sets environment variables
# before anything else is configured in the Rails application.
gem 'figaro'

# The gem uses interceptors which were introduced in Rails 3 and tweaks all mails
# which are delivered and adds a plain text part to them and inlines all CSS
# rules into the HTML part.
gem 'roadie', '=2.3.4'
