describe 'LifeOnCharts Directives', ->

  beforeEach module('lifeOnChartsApp')

  describe 'Actionable Directive', ->
    {$compile, $scope, elm} = [null, null, null]

    beforeEach inject (_$compile_, _$rootScope_)->
      $compile = _$compile_
      $scope = _$rootScope_.$new()
      html = '<div actionable><div class="actions"></div></div>'
      elm = angular.element(html)
      $compile(elm)($scope)

    it 'shows actions when element has the focus', ->
      elm.trigger 'mouseover'
      expect(elm.find('.actions').css('display')).toBe('block')

    it 'hides actions when element loose the focus', ->
      elm.trigger 'mouseout'
      expect(elm.find('.actions').css('display')).toBe('none')

    it 'shows actions when selected element loose the focus', ->
      $scope.selected = true
      elm.trigger 'mouseout'
      expect(elm.find('.actions').css('display')).toBe('block')

  describe 'Commentable directive', ->

    {$compile, $scope, elm} = [null, null, null]

    beforeEach inject (_$compile_, _$rootScope_,_$httpBackend_) ->
      $compile = _$compile_
      $scope = _$rootScope_.$new()
      $httpBackend = _$httpBackend_
      html = '<div commentable><div class="actions"></div></div>'
      elm = angular.element(html)
      $compile(elm)($scope)

    it 'adds comment button to action button panel', ->
      expect(elm.find('.icon-comments').length).toBe(1)

    it 'makes the element', ->

