describe 'LifeOnCharts Controllers', ->

  beforeEach module('lifeOnChartsApp')

  describe 'AchievementsCtrl', ->
    {scope, ctrl, $httpBackend} = [null, null, null]

    beforeEach inject (_$httpBackend_, $rootScope, $controller) ->
      $httpBackend = _$httpBackend_
      $httpBackend.whenGET('/achievements').respond([
        {name:"Achievement1", description: "Description..."},
        {name:"Achievement2", description: "Description..."},
        {name:"Achievement3", description: "Description..."}
      ])

      scope = $rootScope.$new()
      ctrl = $controller('AchievementsCtrl', {$scope: scope})

    it 'should create "achievement list" with 3 items',->
      $httpBackend.flush()
      expect(scope.achievements.length).toBe(3);
      expect(scope.achievements[0].name).toMatch("Achievement1")

    it 'should update status on tabSelected events',->
      scope.$broadcast 'tabSelected', { selection: 'test' }
      expect(scope.selection).toBe('test')

    it 'should broadcast actionLoadComments on actionOpenDetails',->
      scope.$on 'actionLoadComments', -> scope.flag = true
      scope.$broadcast 'actionOpenDetails', { actionModelType: 'Achievement' }
      expect(scope.flag).toBe(true)
