describe 'dashboardApp', ->
	
	describe 'Controllers Module', ->

		beforeEach angular.mock.module('controllers')
		beforeEach inject(($httpBackend) ->
			$httpBackend.whenGET('/task_categories').respond([{"created_at":"2013-03-26T15:52:02Z","id":1,"name":null,"parent":null,"updated_at":"2013-03-26T15:52:02Z"},{"created_at":"2013-03-26T15:52:02Z","id":1,"name":null,"parent":null,"updated_at":"2013-03-26T15:52:02Z"},{"created_at":"2013-03-26T15:52:02Z","id":1,"name":null,"parent":null,"updated_at":"2013-03-26T15:52:02Z"}])
		)

		describe 'ControlPanelCtrl', ->
			{scope, ctrl} = [null, null]

			beforeEach inject ($rootScope,$injector,$controller)->
				scope = $rootScope.$new()
				ctrl = $controller "ControlPanelCtrl", { $scope: scope }

			it 'should create "category list" with 3 items',->
				expect(scope.taskCategoryList.length).toBe(0);
				#expect(typeof(scope.returnEmail) == 'function')
