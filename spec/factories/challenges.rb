# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :challenge do
    title "MyString"
    description "MyString"
    image_url "MyString"
  end
end
