FactoryGirl.define do
  factory :user do |f|
    f.sequence(:first_name) { |n| "User#{n}" }
    f.nickname { |u| u.first_name.downcase }
    f.last_name "factory"
    f.email { |u| "#{ u.first_name.downcase }.factory@test.com" }
  end
end
