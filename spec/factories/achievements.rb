# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :achievement do |f|
    f.sequence(:title) { |n| "Achievement#{n}" }
    f.description { |a| "Description of achievement #{a.title}" }
  end
end
