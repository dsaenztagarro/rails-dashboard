require 'spec_helper'

describe Achievement do
  before :each do
    @achievement = FactoryGirl.create :achievement
  end

  it "is invalid without a title" do 
    @achievement.title = nil 
    @achievement.should_not be_valid
  end

  it "has an unique title" do
    achievement = FactoryGirl.build(:achievement, title: @achievement.title)
    achievement.should_not be_valid
  end

  it "is invalid without a description" do
    @achievement.description = nil 
    @achievement.should_not be_valid
  end

  it "is created by an user"
end
