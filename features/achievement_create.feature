Feature: Manage Achievement
  In order to plan achievements for my life
  As an user
  I want to create and manage achievements

#  Background:
#    Given I am a new, authenticated user
#
  @wip
  @javascript
  Scenario: Achievement List
    Given a user has created the following achievements:
      | Baruco - Barcelona Ruby |
      | PIWeek                  |
      | Rails Conference        |
    When I go to the home page
    Then I should see "Baruco - Barcelona Ruby"
    And I should see "PIWeek"
    And I should see "Rails Conference"


