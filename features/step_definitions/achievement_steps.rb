Given /^a user has created the following achievements:$/ do |titles|
  @titles ||= []
  titles.raw.each do |title|
    @titles << FactoryGirl.create(:achievement, title: title[0])
  end
end
