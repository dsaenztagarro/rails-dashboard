Given /^I am a new, authenticated user$/ do
    @user = FactoryGirl.create(:user, password: "01234567")

    visit '/users/sign_in'
    fill_in "user_email", :with => @user.email
    fill_in "user_password", :with => @user.password
    click_button "Sign In"
end
