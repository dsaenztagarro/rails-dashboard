require 'factory_girl'

FactoryGirl.define do
  factory :user do
    first_name "David"
    last_name "Saenz"
    email "david.saenz.tagarro@gmail.com"
  end

end

