require 'rubygems'
require 'spork'
#uncomment the following line to use spork with the debugger
#require 'spork/ext/ruby-debug'

Spork.prefork do

  ENV["RAILS_ENV"] ||= "test"
  require File.expand_path(File.dirname(__FILE__) + '/../../config/environment')

  require 'cucumber'
  require 'cucumber/formatter/unicode'
  require 'cucumber/rails'
  require 'cucumber/rails/world'
  require 'cucumber/rails/rspec'

  require 'capybara/rails'
  require 'capybara/cucumber'
  require 'capybara/poltergeist'
  require 'capybara/session'

  require 'rspec/expectations'

  require 'active_record'

  Capybara.default_selector = :css

  ActionController::Base.allow_rescue = false

  begin
    DatabaseCleaner.strategy = :transaction
  rescue NameError
    raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
  end

  Cucumber::Rails::Database.javascript_strategy = :truncation

  Cucumber::Rails::World.use_transactional_fixtures = false
end

Spork.each_run do
  Dir["../../spec/factories/*.rb"].each { |file| require_relative file }
  FactoryGirl.reload

  Capybara.default_driver = :poltergeist
  Capybara.register_driver :poltergeist do |app|
    options = {
      :js_errors => false,
      :timeout => 120,
      :debug => false,
      :phantom_options => ['--load-images=no', '--disk-cache=false', '2>&1> /tmp/phantom.log'],
      :inspector => true,
    }
    Capybara::Poltergeist::Driver.new(app, options)
  end
  Capybara.javascript_driver = :poltergeist
end

