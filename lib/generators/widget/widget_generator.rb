class WidgetGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)
  argument :widget_type, :type => :string, :required => true
  argument :widget_name, :type => :string, :required => true
  
  def generate_widget
    template "layout.html.erb", "app/views/layouts/widgets/#{file_name}.html.erb"  
  end
  
  private  
  def file_name  
    "#{@widget_type}_#{@widget_name}".underscore  
  end      
end