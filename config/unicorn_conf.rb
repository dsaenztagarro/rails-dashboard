root = "/home/railsuser/apps/rails-dashboard/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.rails-dashboard.sock"
worker_processes 2
timeout 30