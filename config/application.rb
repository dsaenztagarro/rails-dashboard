require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module RailsApp
  class Application < Rails::Application
    config.encoding = "utf-8"
    config.filter_parameters += [:password]
    config.active_support.escape_html_entities_in_json = true
    config.active_record.whitelist_attributes = true
   
    config.assets.initialize_on_precompile=false
    config.assets.enabled = true
    config.assets.version = '1.0'
    config.assets.paths << "#{Rails.root}/app/assets"
    config.assets.paths << "#{Rails.root}/vendor/assets"
    config.assets.paths << "#{Rails.root}/vendor/assets/images"

    # don't generate RSpec tests for views and helpers
    config.generators do |g|
      g.test_framework :rspec, fixture: true
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.view_specs false
      g.helper_specs false
      g.stylesheets = false
      g.javascripts = false
      g.helper = false
    end
    
    # Revert to the old numbering scheme
    #config.active_record.timestamped_migrations = false
    
    # Put jquery library at the top of javascript files
    config.action_view.javascript_expansions[:defaults] = %w('jquery rails')
            
  end
end
