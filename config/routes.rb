RailsApp::Application.routes.draw do

  resources :public, only: [:home, :test] do
    collection do
      get 'home'
      get 'test'
    end
  end

  resources :achievements do
    resources :comments
  end

  resources :challenges do
    resources :comments, only: [:index, :create]
  end

  resources :comments, only: [:index, :show, :new]

  devise_for :users, controllers: {
    omniauth_callbacks: "omniauth_callbacks",
    registrations: "registrations"
  }

  resources :users

  get 'signup', to: 'users#new', as: 'signup'

  root :to => 'public#home'

  mount JasmineRails::Engine => "/specs" if defined?(JasmineRails)
end
