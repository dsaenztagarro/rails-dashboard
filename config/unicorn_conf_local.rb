working_directory "/vagrant"
pid "/vagrant/tmp/pids/unicorn.pid"
stderr_path "/vagrant/log/unicorn.stderr.log"
stdout_path "/vagrant/log/unicorn.stdout.log"

listen "/tmp/unicorn.todo.sock"
worker_processes 2
timeout 30
